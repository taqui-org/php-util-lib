<?php
	function mail_base( $source, $file, $sql, $title, $addresses, $fromSuffix = '' )
	{
		$info = pathinfo( $source );
		
		$dir1 = $info['dirname'].'/tmp';
		create_dir( $dir1 );
		
		$dir2 = $dir1.'/'.$info['filename'];
		create_dir( $dir2 );
		
		$csv = $dir2.'/'.$file.'.csv';
		$file_csv = fopen( $csv, 'w' );
		
		if( !$file_csv )
		{
			echo "\nERRO AO CRIAR O ARQUIVO: ".$csv."\n";
			exit( 3 );
		}
		
		fwrite( $file_csv, "\xEF\xBB\xBF" );
		
		$set = execute_select( $sql );
		$rows = $set['rows'];
		$result = $set['result'];
		
		$header = true;
		
		while( $row = $result->fetch_array( MYSQLI_ASSOC ) )
		{
			if( $header )
			{
				$header = false;
				$separator = '';
				
				foreach( $row as $name => $value )
				{
					fwrite( $file_csv, $separator );
					fwrite( $file_csv, strtr( to_utf8( $name, true ), ';', ',' ) );
					$separator = ';';
				}
				fwrite( $file_csv, "\r\n" );
			}
			
			$separator = '';
			foreach( $row as $value )
			{
				fwrite( $file_csv, $separator );
				fwrite( $file_csv, strtr( to_utf8( $value, true ), ';', ',' ) );
				$separator = ';';
			}
			fwrite( $file_csv, "\r\n" );
		}
		
		if( !fclose( $file_csv ) )
		{
			echo "\nERRO AO FECHAR O ARQUIVO: ".$csv."\n";
			exit( 4 );
		}
		
		
		$zip = $dir2.'/'.$file.'.zip';
		exec( 'zip -j -9 "'.$zip.'" "'.$csv.'"', $out, $status );
		
		if( $status !== 0 )
		{
			echo "\nERRO AO CRIAR O ARQUIVO: ".$zip."\n";
			exit( 4 );
		}
		
		mail_report( $title, new Attachment( basename( $zip ), $zip ), $addresses, $fromSuffix );
	}
	
	
	function mail_result( $attachments, $title, $addresses, $fromSuffix = '' )
	{
		if( $attachments instanceof Attachment )
			$attachments = array( $attachments );
		
		if( is_array( $attachments ) )
		{
			foreach( $attachments as $attachment )
			{
				$attachment->source = select_multiple_rows( $attachment->source );
				if( count( $attachment->source ) > 0 )
				{
					$header = array_keys( reset( $attachment->source ) );
					$header[''] = 1;
					
					$attachment->source = array_merge( array( $header ), $attachment->source );
				}
			}
		}
		
		mail_report( $title, $attachments, $addresses, $fromSuffix );
	}
	
	
	function mail_report( $title, $attachments, $addresses, $fromSuffix = '' )
	{
		$mail = new PHPMailer();
		$mail->IsSMTP();
	
		if( config_get( 'utf-8' ) )
			$mail->CharSet = 'utf-8';
		else
			$mail->CharSet = 'iso-8859-1';
	
		$mail->Host = 'smtp.okto.br';
		$mail->Port = '25';
		
		$mail->IsHTML( true );
		
		if( $attachments instanceof Attachment )
			$attachments = array( $attachments );
		
		
		$text = '';
		$html = '';
		
		if( is_array( $attachments ) )
		{
			foreach( $attachments as $attachment )
			{
				if( @file_exists( $attachment->source ) )
				{
					$mail->Body = 'Em anexo.';
					$mail->AltBody = $mail->Body;
					
					$mail->AddAttachment( $attachment->source, $attachment->name, 'base64', 'application/'.preg_replace('/^.*\\.([^\\.]+)$/', '\\1', $attachment->name ) );
				}
				else
				{
					if( !is_array( $attachment->source ) )
					{
						echo 'CONTEÚDO INVÁLIDO INFORMADO! ABORTANDO: '.$attachment->source."\n";
						exit( 2 );
					}
					
					if( count( $attachment->source ) < 2 )
					{
						echo 'SOMENTE '.count( $attachment->source )." LINHA(S) RETORNADAS! PULANDO!\n";
						continue;
					}
					
					$csv = query_result_to_csv( $attachment->source );
					if( !preg_match( '/\\.csv$/i', $attachment->name ) )
						$attachment->name = $attachment->name.'.csv';
					
					if( $text != '' )
					{
						$text .= "\n\n";
						$html .= "<br/>\n<br/>\n";
					}
					
					$text .= query_result_to_text( $attachment->source );
					$html .= query_result_to_html( $attachment->source, true );
					
					$mail->AddStringAttachment( $csv, $attachment->name, 'base64', 'text/csv', $attachment->name );
				}
			}
		}
		
		if( $text != '' )
		{
			$mail->Body = $html;
			$mail->AltBody = $text;
		}
		
		if( $mail->AltBody == '' )
		{
			echo "NENHUMA TABELA GERADA! ABORTANDO!\n";
			exit( 1 );
		}
	
		$mail->Subject = $title.' - Gerado em '.date('d/m/Y');
	
		$mail->From = config_get( 'From-Address' );
		$mail->FromName = config_get( 'From-Name' );
		if( $fromSuffix != '' )
			$mail->FromName .= ' - '.$fromSuffix;
		
		foreach( $addresses as $address )
			$mail->AddAddress( $address );
		
		$mail->SetLanguage('br', 'lib/phpmailer/');
	
		echo "\n".$mail->Subject."\n";
		echo "\n".$mail->AltBody."\n\n";
	
		if( !$mail->Send() )
		{
			echo "\nERRO AO ENVIAR O EMAIL!\n";
			exit( 2 );
		}
		else
			echo "SUCESSO!\n\n";
	}
	
	
	function create_dir( $dir )
	{
		if( !file_exists( $dir ) )
		{
			if( !mkdir( $dir ) )
			{
				echo "\nERRO AO CRIAR O DIRETORIO: ".$dir."\n";
				exit( 255 );
			}
		}
	}
	
	class Attachment
	{
		var $name;
		
		var $source;
		
		
		function __construct( $name, $source )
		{
			$this->name = $name;
			$this->source = $source;
		}
	}
?>