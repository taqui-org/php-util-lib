<?php
	error_reporting(E_ALL ^ E_NOTICE ^ E_DEPRECATED);
	ini_set('display_errors', true);
	
	date_default_timezone_set('America/Sao_Paulo');
	
	
	if(!isset($_SERVER['PATH_INFO']) && isset($_SERVER['ORIG_PATH_INFO']) )
		$_SERVER['PATH_INFO'] = $_SERVER['ORIG_PATH_INFO'];
	
	if( !chdir( dirname( __FILE__ ) ) )
	{
		trigger_error('Unable to access: '.dirname( __FILE__ ), E_USER_ERROR);
		return;
	}
	
	lib_include('common');
	lib_include('logging');
	lib_include('parsing');
	lib_include('phpmailer');
	
	config_set('utf-8', true);
	
	
	
	function lib_include($dir, $include_path = false)
	{
		$dir = dirname(__FILE__)."/$dir";
		$real = realpath($dir);
		if($real === false)
		{
			trigger_error('Unable to find: '.$dir, E_USER_ERROR);
			return;
		}
		
		if(is_dir($real))
		{
			if($include_path)
				set_include_path($real.PATH_SEPARATOR.get_include_path());
		}
		
		auto_include($real);
	}
	
	
	
	function auto_include($path)
	{
		$real = realpath($path);
		if($real === false)
		{
			trigger_error('Unable to find: '.$path, E_USER_ERROR);
			return;
		}
		
		if(is_file($real))
		{
			require_once($real);
			return;
		}
		
		if(preg_match('@/$@', $real) === 0)
			$real .= '/';
		
		$dir = opendir($real);
		if($dir === false)
			trigger_error('Unable to read: '.$real, E_USER_ERROR);
		else
		{	
			while(($file = readdir($dir)) !== false)
			{
				if(preg_match('/^\\./', $file))
					continue;
				
				if(is_dir($real.$file) && !is_link($real.$file))
					auto_include($real.$file);
				else
				{
					if(!preg_match('/\\.php$/', $file))
						continue;
					else
						require_once($real.$file);
				}
			}
			
			//set_include_path($path.PATH_SEPARATOR.get_include_path());
		}
	}
?>