<?php
	function get_parse_http_error($set = null, $set_full = null)
	{
		static $error = false;
		
		if($set !== null && (!is_bool($set) || $set_full !== null))
			$error = array($set, $set_full);
		
		if($set === true)
			return @$error[0]."\n".@$error[1];
		else
			return @$error[0];
	}
	
	
	
	function parse_http_multiline(&$url_ref, $pattern, $multiple, $headers = null, $parse_retries = 0, $retry_pattern = '/^/', $redirect = false, $read_timeout = 10, $cache = true)
	{
		return parse_http($url_ref, $pattern, $multiple, $headers, $parse_retries, $retry_pattern, $redirect, $read_timeout, $cache, true);
	}
	
	
	
	function parse_http($url, $pattern, $multiple, $headers = null, $parse_retries = 0, $retry_pattern = '/^/', $redirect = false, $read_timeout = 10, $cache = true, $keep_break = false)
	{
		$parse_retries += 0;
		
		for($i=0; true; $i++)
		{
			if($cache)
				$content = config_get('http_cache', $url);
			
			if(@$content === null)
			{
				$response = url_request($url, $headers, null, 15, $read_timeout, $redirect);
				
				if(!$response->request_success || $response->status_code !== 200)
					$content = false;
				else
				{
					$content = $response->body_decoded;
					if(@$response->cookie !== null)
						$headers['Cookie'] = $response->cookie;
				}
			}
			
			if(!is_string($content))
			{
				config_add('http_cache', false, $url);
				get_parse_http_error('Request error: '.$url.' <= '.$response->request_error_msg.($response->status_code.' '.$response->status_msg), false);
				return false;
			}
			else
			{
				config_add('http_cache', $content, $url);
				
				if($multiple)
					$result = preg_match_all($pattern, $content, $matches, PREG_SET_ORDER);
				else
				{
					$result = preg_match($pattern, $content, $matches);
					$matches = array($matches);
				}
				
				if(!$result)
				{
					if($i >= $parse_retries || !is_string($retry_pattern) || !preg_match($retry_pattern, $content))
					{
						get_parse_http_error('Parse error ('.$i.'): '.$url.' <= '.$pattern, $content);
						
						if($multiple)
							return array();
						else
							return false;
					}
					else
						config_add('http_cache', null, $url);
				}
				else
				{
					get_parse_http_error(false, false);
					
					foreach($matches as $key => $info)
					{
						$line = $info[0];
						$info = array_slice($info, 1);
						foreach($info as $index => $text)
							$info[$index] = parsedFixer($text, $keep_break);
						
						$info[-1] = parsedFixer( preg_replace('@(?<! )</@', ' </', $line), $keep_break);
						$matches[$key] = $info;
					}
					
					if($multiple)
						return $matches;
					else
						return $matches[0];
				}
			}
		}
	}
	
	
	
	function parsedFixer($text, $keep_break)
	{
		$parsed = from_html($text, $keep_break);
		
		if($parsed === '')
			$parsed = null;
		
		return $parsed; 
	}
	
	
	
	function regexConcatenator($limit, $flags, $array, $concatenator = '.*?')
	{
		$separator = '';
		$regex = '';
		
		$regex .= $limit;
		foreach($array as $current)
		{
			$regex .= $separator;
			$regex .= $current;
			$separator = $concatenator;
		}
		$regex .= $limit;
		$regex .= $flags;
		
		return $regex;
	}
	
	
	
	function getLongest()
	{
		$name = null;
		foreach(func_get_args() as $current)
		{
			if(is_string($current) && ($name === null || strlen($current) > strlen($name)))
				$name = $current;
		}
		
		return $name;
	}
?>