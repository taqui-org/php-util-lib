#!/bin/bash

app="$1"
logserver="$2"
max="$3"

if [ "${app}" == "" ]
then
	app="script-php"
	
	if [ "${#app}" -gt 32 ]
	then
		app="${app:0:32}"
	fi
fi

if [ "${logserver}" == "" ]
then
	logserver="stewart"
fi

if [ "${max}" == "" ]
then
	max=65000
fi

sequence=0

while read -r line
do
	echo "$line"
	
	first=1
	
	while [ 1 ]
	do
		if [ "${#line}" -gt "${max}" ]
		then
			has_more=1
			more="${line:${max}}"
			line="${line:0:${max}}"
		else
			has_more=0
			more=""
		fi
		
		(( sequence++ ))
		thread="${sequence}"
		(( max_thread = 30 - ${#app} ))
		
		if [ ${#thread} -gt ${max_thread} ]
		then
			(( max_thread = ${#thread} - max_thread ))
			thread="${thread:${max_thread}}"
		fi
		
		message="`printf '<11>1 %s %s %s %s %s %s%s%s' "$(date '+%Y-%m-%dT%H:%M:%S.000%:z')" "$(hostname)" "${app}" "${thread}" "${sequence}" "${line}" "$( if [ ${first} == 0 -a ${has_more} == 1 ]; then echo -n '-----'; fi )" "$( if [ $has_more == 1 ]; then echo -n '-----'; fi )"`"
		echo -n "${message}" > /dev/udp/"${logserver}"/514
		
		first=0
		
		if [ "$has_more" == "0" ]
		then
			break
		else
			line="${more}"
		fi
	done
done