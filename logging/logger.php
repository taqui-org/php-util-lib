<?php
	class BasicLogger
	{
		var $out;
		var $echo;
		var $level;
		
		
		function BasicLogger($file, $level = 'ALL', $echo = false)
		{
			switch(gettype($level))
			{
				case 'integer':
					$this->level = $level;
					break;
					
				default:
					switch(strtoupper($level))
					{
						case 'ALL':
							$this->level = 0;
							break;
						case 'TRACE':
							$this->level = 1000;
							break;
						case 'DEBUG':
							$this->level = 2000;
							break;
						case 'INFO':
							$this->level = 3000;
							break;
						case 'WARN':
							$this->level = 4000;
							break;
						case 'ERROR':
							$this->level = 5000;
							break;
						case 'FATAL':
							$this->level = 6000;
							break;
						case 'OFF':
							$this->level = 2147483647;
							break;
						default:
							$this->level = 0;
					}
			}
			
			if($echo === true)
				$this->echo = true;
			
			if($file === false)
				$this->out = null;
			else if($file === null || $file === true)
				$this->out = 'php://stderr';
			else if(gettype($file) == 'resource')
				$this->out = $file;
			else
				$this->out = $file;
		}
		
		
		function log($level, $message, $echo = null)
		{
			if(gettype($this->out) != 'resource')
			{
				if(gettype($this->out) == 'string')
					$this->out = fopen($this->out, 'ab');
			}
			
			$message = '['.sprintf('%-5s', $level).'] ('.date('Y-m-d H:i:s').') '.$message."\n";
			
			if(gettype($this->out) === 'resource')
				fwrite($this->out, $message);
			
			if(($echo === null && $this->echo === true) || $echo === true)
				echo $message;
		}
		
		
		function all($message, $echo = null)
		{
			if($this->level <= 0)
				$this->log('ALL', $message, $echo);
		}
		
		
		function trace($message, $echo = null)
		{
			if($this->level <= 1000)
				$this->log('TRACE', $message, $echo);
		}
		
		
		function debug($message, $echo = null)
		{
			if($this->level <= 2000)
				$this->log('DEBUG', $message, $echo);
		}
		
		
		function info($message, $echo = null)
		{
			if($this->level <= 3000)
				$this->log('INFO', $message, $echo);
		}
		
		
		function warn($message, $echo = null)
		{
			if($this->level <= 4000)
				$this->log('WARN', $message, $echo);
		}
		
		
		function error($message, $echo = null)
		{
			if($this->level <= 5000)
				$this->log('ERROR', $message, $echo);
		}
		
		
		function fatal($message, $echo = null)
		{
			if($this->level <= 6000)
				$this->log('FATAL', $message, $echo);
		}
	}
?>