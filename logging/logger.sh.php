<?php
	function auto_syslog( $app = '', $host = 'stewart', $max = 65000 )
	{
		$execute = array_search( '--execute', @$_SERVER['argv'] ) !== false;
		$log = array_search( '--log', @$_SERVER['argv'] ) !== false;
		
		if( $execute )
			return;
		
		exit( passthru( $_SERVER['SCRIPT_FILENAME'].' --execute 2>&1 | '.dirname( $_SERVER['SCRIPT_FILENAME'] )."/lib_include/logger.sh '$app' '$host' '$max'" ) ) ;
	}
?>