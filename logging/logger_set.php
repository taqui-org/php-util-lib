<?php
	function logger($replacement = null)
	{
		static $logger = null;
		
		if($replacement !== null)
			$logger = $replacement;
		
		return $logger;
	}
	
	
	
	function logger_get()
	{
		return logger();
	}
	
	
	
	function logger_set($logger)
	{
		return logger($logger);
	}
?>