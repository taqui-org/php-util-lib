<?php
	function log_all($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->all($message, $echo);
	}
	
	
	function log_trace($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->trace($message, $echo);
	}
	
	
	function log_debug($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->debug($message, $echo);
	}
	
	
	function log_info($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->info($message, $echo);
	}
	
	
	function log_warn($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->warn($message, $echo);
	}
	
	
	function log_error($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->error($message, $echo);
	}
	
	
	function log_fatal($message, $echo = null)
	{
		$logger = logger_get();
		if(gettype($logger) == 'object')
			$logger->fatal($message, $echo);
	}
?>