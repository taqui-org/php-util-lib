<?php
	function get_url($url = true, $relative = false, $include_password = false)
	{
		$parsed = get_parsed_url($url, $relative, $include_password);
		if(is_object($parsed))
			return $parsed->url;
		else
			return false;
	}
?>