<?php
	function &select_single_value($sql, $conn=null)
	{
		return selectSingleValue($sql, $conn);
	}
	
	
	
	function &selectSingleValue($sql, $conn=null)
	{
		$row = selectOneRow($sql, $conn, false);
		
		if(is_array($row))
			$row = reset($row);
			
		return $row;
	}
?>