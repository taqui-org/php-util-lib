<?php
	function apache_not_found()
	{
		layout_clear();
		
		header('HTTP/1.1 404 Not Found');
		header('Content-Type: text/html;charset=utf-8');
		
		?>
			<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
			<html>
				<head>
					<title>404 Not Found</title>
				</head>
				<body>
					<h1>Not Found</h1>
					<p>The requested URL <?=toHTML(get_path())?> was not found on this server.</p>
					<hr>
					<?=$_SERVER['SERVER_SIGNATURE']?>
				</body>
			</html>
		<?php
			
		die();
	}
?>