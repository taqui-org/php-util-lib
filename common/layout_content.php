<?php
	function layout_content($replacement = null, $id = null, $eval = true, $headers = null)
	{
		static $layout_prepared = array();
		
		if($replacement === true)
			$layout_prepared = array();
		
		if(!is_string($id) && !is_int($id) && !is_bool($id))
			$id = 0;
		
		if(is_array($replacement) || is_string($replacement))
		{
			$layout_prepared[$id] = array(
				'content' =>$replacement,
				'eval' => !!$eval,
				'headers' => $headers
			);
		}
		
		return @$layout_prepared[$id];
	}
?>