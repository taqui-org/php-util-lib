<?php
	function refresh($seconds, $die = true, $url = null, $append = '')
	{
		if($url === null || $url === '')
			$redirect = null;
		else
			$redirect = redirect($url, $append, true);
		
		header('Refresh: '.$seconds.($redirect === null ? '' : ';url='.$redirect));
		
		if($die)
			die();
	}
?>