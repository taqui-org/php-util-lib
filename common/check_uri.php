<?php
	function check_uri_file($file, $uri = null)
	{
		return check_uri_path($file, $uri, $path, $query_string, $anchor);
	}
	
	
	
	function check_uri_path($file, $uri = null, &$path, &$query_string, &$anchor)
	{
		if(gettype($uri) != 'string')
			$uri = $_SERVER['REQUEST_URI'];
		
		$path = get_path_from_uri($uri, $query_string, $anchor);
		
		if(preg_match('@/'.preg_quote($file, '@').'$@', $path) === 1)
			return true;
		else
			return false;
	}
?>