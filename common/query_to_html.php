<?php
	function query_result_to_html(&$resultado, $alternar = false, $css = false)
	{
		return queryResult2html($resultado, $alternar, $css);
	}
	
	
	
	function queryResult2html(&$resultado, $alternar = false, $css = false)
	{
		if(gettype($resultado) != 'array' || count($resultado) == 0)
			return '';
		
		$echo = '';
		
		if(gettype($css) == 'string')
		{
			$echo .= $css;
			$css = true;
		}
		else
		{
			$css = $css === true;
			
			if($css)
				$echo .= '
					<style type="text/css">
						table.report
						{
							font-family: "Adobe Helvetica", Arial, Helvetica, Sans-serif;
							font-size: 10pt;
							border-style: solid;
							border-width: 3px;
							border-color: #000000;
						}
						
						table.report td
						{
							border-style: solid;
							border-width: 1px;
							border-color: #000000;
							padding-top: 1px;
							padding-bottom: 1px;
							padding-left: 8px;
							padding-right: 8px;
						}
						
						table.report td.right
						{
							text-align: right;
						}
						
						table.report .alternative
						{
							background-color: #F0F0F0;
						}
						
						table.report .highlight
						{
							background-color: #808080;
							color: #FFFFFF;
							font-weight: bold;
						}
					</style>
				';
		}
		
		
		$alternativa = false;
		
		if($css)
			$echo .= '<table class="report">'."\n";
		else
			$echo .= '<table style="border-color: #000000; border-style: solid; border-width: 3px; font-size: 10pt; font-family: \'Adobe Helvetica\', Arial, Helvetica, Sans-serif;">'."\n";
		
		for($registro = reset($resultado); key($resultado) !== null; $registro = next($resultado))
		{
			if(gettype($registro) != 'array')
				continue;
			
			if(count($registro) == 0)
				continue;
			
			
			$echo .= "\t<tr";
			
			if($registro[''])
			{
				if($css)
					$echo .= ' class="highlight"';
				else
					$echo .= ' style="background-color: #808080; font-weight: bold"';
			}
			else
			{
				if($alternativa) 
				{
					if($css)
						$echo .= ' class="alternative"';
					else
						$echo .= ' style="background-color: #F0F0F0"';
				}
				
				if($alternar)
					$alternativa = !$alternativa;
			}
			
			
			$echo .= ">\n";
			
			$aDireita = false;
			$nColuna = 0;
			$span = 1;
			$coluna = null;
			$continua = true;
			
			for($valor = reset($registro); ($index = key($registro)) !==null || $continua; $valor = next($registro))
			{
				if($index === '')
					continue;
				
				if($index === null)
				{
					$continua = false;
					$valor = $coluna;
				}
				
				
				if($valor !== null)
				{
					if($aDireita)
						$coluna = $valor;
					
					if($coluna !== null)
					{
						$echo .= "\t\t<td";
						
						if(!$css)
							$echo .= ' style="border-style: solid; border-width: 1px; border-color: #000000; padding: 1px 8px;';
						
						if(!$css && $registro[''])
							$echo .= '; color: #FFFFFF';
						
						if($aDireita || preg_match('/^[0-9]+([,.][0-9]+)*%?$/', $coluna) === 1)
						{
							if($css)
								$echo .= ' class="right"';
							else
								$echo .= '; text-align: right';
						}
						
						if(!$css)
							$echo .= '"';
						
						if($span > 1)
							$echo .= ' colspan="'.$span.'"';
						
						$echo .= ">".toHtml($coluna, false, true);
						$echo .= "</td>\n";
						
						
						$coluna = null;
						$span = 1;
					}
					
					if(!$aDireita)
						$coluna = $valor;
					
					
					$aDireita = false;
				}
				else
				{
					$span++;
					
					if($nColuna == 0)
						$aDireita = true;
				}
				
				
				$nColuna++;
			}
			
			
			$echo .= "\t</tr>\n";
		}
		
		return $echo .= "</table>\n";
	}
?>