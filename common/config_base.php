<?php
	function config_base($name, $value = null, $write = false)
	{
		static $config = array();
		
		if($write)
			$config[$name] = $value;
		
		return @$config[$name];
	}
?>