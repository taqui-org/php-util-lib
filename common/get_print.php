<?php
	function get_print($target, $embed = false, $input_utf8 = null, $output_utf8 = null)
	{
		switch(gettype($target))
		{
			case 'string':
				$text = $target;
				break;
			default:
				ob_start();
				var_dump($target);
				$text = ob_get_clean();
		}
		
		if($input_utf8 === null)
			$input_utf8 = is_utf8($text); 
		
		if($output_utf8 === null)
			$output_utf8 = config_get('utf-8');
		
		if($output_utf8 && !$input_utf8)
			$text = convert_charset($text, 'cp1252', 'utf-8');
		
		if(!$output_utf8 && $input_utf8)
			$text = convert_charset($text, 'utf-8', 'cp1252');
			
		if(!$embed)
			return $text;
		else
			return '<pre wrap="" style="text-align: left">'.toHTML($text, false, $output_utf8).'</pre>';
	}
?>