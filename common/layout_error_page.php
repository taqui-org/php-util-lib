<?php
	function layout_error_page($text, $plain = false, $debug = '', $input_utf8 = null, $output_utf8 = null)
	{
		if($plain !== 'html')
		{
			if(is_bool(config_get('plain')))
				$type = config_get('plain') ? 'plain' : 'html';
			else
			{
				if(php_sapi_name() != 'cli')
					$type = 'html';
				else
					$type = 'plain';
			}
		}
		
		if($output_utf8 === null)
			$output_utf8 = config_get('utf-8');
		
		$suffix = ';charset='.($output_utf8 ? 'utf-8' : 'iso-8859-1');
		if(version_compare(phpversion(), '5.0') >= 0)
		{
			$headers = headers_list();
			
			foreach($headers as $header)
			{
				if(preg_match('@^Content-Type: text/(html|plain)(;.*)?$@i', $header, $match) === 1)
				{
					$type = $match[1];
					$suffix = @$match[2];
					
					if(preg_match('/(utf-8|iso-8859-1|cp1252)/i', $suffix, $matches) === 1)
						$output_utf8 = strtolower($matches[1]) === 'utf-8';
					else
					{
						$output_utf8 = false;
						$input_utf8 = false;
					}
					
					break;
				}
			}
		}
		
		@ob_end_clean();
		
		if(!headers_sent())
			header('HTTP/1.0 500 Internal Server Error');
		
		if(function_exists('logger_get'))
		{
			$logger = logger_get();
			if($logger !== null)
				$logger->error($text."\nDebug: ".$debug, false);
		}
		
		if($type === 'plain')
		{
			layout_clear();
			
			if(!headers_sent())
				header('Content-Type: text/plain'.$suffix);
			
			$text = get_print($text, false, $input_utf8, $output_utf8);
			
			if(config_get('debug'))
				$text .= "\n".get_print($debug, false, $input_utf8, $output_utf8);
		}
		else
		{
			if(!headers_sent())
				header('Content-Type: text/html'.$suffix);
			
			$hook  = config_get('on-error');
			if(is_string($hook))
				@include($hook);
				
			if(layout_content(null, 'error') !== null)
				layout('error');
			else
				layout();
			
			if($plain !== 'html')
			{
				if($plain !== true)
				{
					$text = get_print($text, false, $input_utf8, $output_utf8);
					$text = toHTML($text, true, $output_utf8);
				}
				else
					$text = get_print($text, true, $input_utf8, $output_utf8);
			}
			
			if(config_get('debug'))
				$text .= get_print($debug, true, $input_utf8, $output_utf8);
		}
		
		if(error_reporting() > 0)
			die($text);
		else
			die(666);
	}
?>