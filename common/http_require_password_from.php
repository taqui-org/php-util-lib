<?php
	function require_password_from($users, $usersIp = null, $title = null)
	{
		if(!isset($_SERVER['PHP_AUTH_USER']))
		{
			$to_check = array();
			
			foreach($_SERVER as $name => $value)
			{
				if(preg_match('/^(HTTP_AUTHORIZATION|(REDIRECT_)*REMOTE_USER)$/', $name, $matches ) === 1)
					$to_check[$matches[1] == 'HTTP_AUTHORIZATION' ? 0 : strlen($name)] = $value;
			}
			
			ksort($to_check);
			
			foreach($to_check as $value)
			{
				if(preg_match('@^Basic ([A-Z0-9+/=]+)$@i', $value, $matches) === 1)
				{
					list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':', base64_decode($matches[1]), 2);
					break;
				}
			}
		}
		
		if(is_array($users))
		{
			if(isset($_SERVER['PHP_AUTH_USER']))
			{
				$user = strtolower($_SERVER['PHP_AUTH_USER']);
				$md5 = $users[$user];
				
				if($md5 !== null && md5(@$_SERVER['PHP_AUTH_PW']) === strtolower($md5))
					return $user;
			}
		}
		
		if(is_array($usersIp))
		{
			foreach($usersIp as $user => $allowed)
			{
				if(!is_string($allowed))
					continue;
				
				if(preg_match('/^[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+$/', $allowed) !== 1)
					$ip = gethostbyname($allowed);
				else
					$ip = $allowed;
				
				if($ip === $_SERVER['REMOTE_ADDR'])
					return $user;
			}
		}
		
		if(is_array($users) && count($users) > 0)
			apache_authorization($title);
		else
			apache_forbidden();
	}
?>