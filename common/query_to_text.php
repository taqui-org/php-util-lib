<?php
	function query_result_to_text($resultado)
	{
		return queryResult2text($resultado);
	}
	
	
	
	function queryResult2text($resultado)
	{
		if(gettype($resultado) != 'array' || count($resultado) == 0)
			return '';
		
		$echo = '';
		$colunas = 0;
		$larguras = array();
		$spans = array();
		$larguras_atual = array();
		
		foreach($resultado as $registro)
		{
			$colunas = max($colunas, count($registro));
			
			$aDireita = false;
			$inicio = 0;
			$i = 0;
			foreach($registro as $index => $valor)
			{
				if($index === '')
					continue;
				
				if($valor !== null)
				{
					$larguras_atual[$i] = strlen(utf8_decode($valor));
					
					if(!$aDireita)
					{
						if($inicio < $i-1)
						{
							$spans[$inicio][$i-1] = max(0, $spans[$inicio][$i-1], $larguras_atual[$i-1], $larguras_atual[$inicio]);
							$larguras_atual[$inicio] = null;
							$larguras_atual[$i-1] = null;
						}
						
						$inicio = $i;
					}
					
					$aDireita = false;
				}
				else
				{
					if($i == 0)
						$aDireita = true;
					
					$larguras_atual[$i] = null;
				}
				
				$i++;
			}
			
			
			if($inicio < $i-1)
			{
				$spans[$inicio][$i-1] = max(0, $spans[$inicio][$i-1], $larguras_atual[$i-1], $larguras_atual[$inicio]);
				$larguras_atual[$inicio] = null;
				$larguras_atual[$i-1] = null;
			}
			
			
			foreach($larguras_atual as $i => $largura)
				$larguras[$i] = max(0, $larguras[$i], $larguras_atual[$i]);
		}
		
		
		
		foreach($spans as $inicio => $sub)
			foreach($sub as $fim => $span)
			{
				$soma = ($fim - $inicio) * 3;
				for($i=$inicio; $i <= $fim; $i++)
					$soma += $larguras[$i];
				
				if($soma < $span)
				{
					for($i=$inicio; $i <= $fim; $i++)
						$larguras[$i] += (int)(($span - $soma) / ($fim - $inicio + 1));
					
					$resto = ($span - $soma) % ($fim - $inicio + 1);
					
					for($i=$fim; $resto > 0; $i--, $resto--)
						$larguras[$i]++;
				}
			}
		
		
		$largura = array_sum($larguras) + (count($larguras) * 3) + 1;
		
		
		if($colunas == 0)
			return '';
		
		
		$separador = str_repeat('-', $largura);
		$separador[0] = '+';
		$separador[$largura-1] = '+';
		$separador .= "\n";
		
		$topo = $separador;
		$nLinha = 1;
		
		foreach($resultado as $registro)
		{
			$linha = '';
			$base = $separador;
			$aDireita = false;
			$nColuna = 0;
			$nulos = array();
			$span = 0;
			
			foreach($registro as $index => $valor)
			{
				if($index === '')
				{
					$nulos[''] = $valor;
					continue;
				}
				
				if($valor !== null && $span > 0 && !$aDireita)
				{
					$linha .= str_repeat(' ', $span);
					$span = 0;
				}
				
				
				$span += $larguras[$nColuna] + 1;
				
				if($valor !== null)
				{
					$nulos[$nColuna] = false;
					
					if(!$aDireita)
					{
						$linha .= '| ';
						$topo[strlen(utf8_decode($linha))-2] = '+';
						$base[strlen(utf8_decode($linha))-2] = '+';
					}
					else
						$span += 2;
					
					if(preg_match('/^[0-9]+([,.][0-9]+)*%?$/', $valor) === 1)
						$aDireita = true;
				}
				else
				{
					$nulos[$nColuna] = true;
					
					if($nColuna == 0)
					{
						$linha .= '| ';
						$aDireita = true;
					}
					else
						$span += 2;
				}
				
				
				if($valor !== null)
				{
					$coluna = str_pad(removeLineLengthBreakers($valor), $span - ($aDireita ? 1 : 0), ' ', $aDireita ? STR_PAD_LEFT : STR_PAD_RIGHT);
					$linha .= $coluna;
					$span -= strlen(utf8_decode($coluna));
					
					$aDireita = false;
				}
				
				$nColuna++;
			}
			
			
			if($span > 0)
			{
				$linha .= str_repeat(' ', $span);
				$span = 0;
			}
			
			$linha .= "|\n";
			$largura_atual = strlen(utf8_decode($linha))-1;
			$topo[$largura_atual-1] = '+';
			$base[$largura_atual-1] = '+';
			
			if($largura != max($largura_atual, $largura_anterior))
				$topo = substr($topo, 0, max($largura_anterior, $largura_atual))."\n";
			
			$largura_anterior = $largura_atual;
			
			
			if($nLinha == 0)
				$echo .= $topo;
			else
			{
				if($nulos != $nulos_anterior)
					$echo .= $topo;
			}
			
			$echo .= $linha;
			
			
			$topo = $base;
			$nulos_anterior = $nulos;
			
			
			$nLinha++;
		}
		
		
		if($largura != $largura_atual)
		{
			$base[$largura_atual-1] = '+';
			$base = substr($base, 0, $largura_atual)."\n";
		}
		
		return $echo .= $base;
	}
?>