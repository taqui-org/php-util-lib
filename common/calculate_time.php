<?php
	function calculate_time($amount, $seconds = true, $complete = false)
	{
		if(!$seconds)
			$amount = (int) $amount / 1000;
		
		$seconds = $amount % 60;
		if($seconds < 10)
			$seconds = '0' . $seconds;
		
		$minutes = (int) ($amount / 60);
		$hours = (int) ($minutes / 60);
		$minutes = $minutes % 60;
		
		if($hours > 0 && !$complete)
			return sprintf('%02d:%02d:%02d', $hours, $minutes, $seconds);
		else
			return sprintf('%02d:%02d', $minutes, $seconds);
	}
?>