<?php
	class ParsedURL
	{
		var $url;
		var $user;
		var $password;
		var $https;
		var $server;
		var $port;
		var $local;
		var $uri;
		var $path;
		var $query;
		var $anchor;
		
		function ParsedURL($https, $server, $port, $path, $query, $anchor, $user, $password)
		{
			$this->https = $https;
			$this->server = $server;
			$this->port = $port;
			$this->path = $path;
			$this->query = $query;
			$this->anchor = $anchor;
			
			$this->user = $user;
			$this->password = $password;
			
			$this->recalculate();
		}
		
		
		function setUrl($value)
		{
			$this->url = $value;
			$this->recalculate();
			
			return $this->url;
		}
		
		function setUser($value)
		{
			$this->user = $value;
			$this->recalculate();
			
			return $this->user;
		}
		
		function setPassword($value)
		{
			$this->password = $value;
			$this->recalculate();
			
			return $this->password;
		}
		
		function setHttps($value)
		{
			$this->https = $value;
			$this->recalculate();
			
			return $this->https;
		}
		
		function setServer($value)
		{
			$this->server = $value;
			$this->recalculate();
			
			return $this->server;
		}
		
		function setPort($value)
		{
			$this->port = $value;
			$this->recalculate();
			
			return $this->port;
		}
		
		function setLocal($value)
		{
			$this->local = get_parsed_url($value.'?#');
			$this->https = $this->local->https;
			$this->user = $this->local->user;
			$this->password = $this->local->password;
			$this->server = $this->local->server;
			$this->port = $this->local->port;
			$this->recalculate();
			
			return $this->local;
		}
		
		function setUri($value)
		{
			$this->path = get_path_from_uri($value, $this->query, $this->anchor);
			$this->recalculate();
			
			return $this->uri;
		}
		
		function setPath($value)
		{
			$this->path = $value;
			$this->recalculate();
			
			return $this->path;
		}
		
		function setQuery($value)
		{
			$this->query = $value;
			$this->recalculate();
			
			return $this->query;
		}
		
		function setAnchor($value)
		{
			$this->anchor = $value;
			$this->recalculate();
			
			return $this->anchor;
		}
		
		
		function getUrl()
		{
			return $this->url;
		}
		
		function getUser()
		{
			return $this->user;
		}
		
		function getPassword()
		{
			return $this->password;
		}
		
		function getHttps()
		{
			return $this->https;
		}
		
		function getServer()
		{
			return $this->server;
		}
		
		function getPort()
		{
			return $this->port;
		}
		
		function getLocal()
		{
			return $this->local;
		}
		
		function getUri()
		{
			return $this->uri;
		}
		
		function getPath()
		{
			return $this->path;
		}
		
		function getQuery()
		{
			return $this->query;
		}
		
		function getAnchor()
		{
			return $this->anchor;
		}
		
		
		function recalculate()
		{
			if(!is_bool($this->https))
				$this->https = preg_match('/^(?:http|)$/', $this->https) === 0;
			
			if(!is_int($this->port))
				$this->port = ($this->port != '' ? (int)$this->port : ($this->https ? 443 : 80));
			
			$this->path .= '';
			if(preg_match('@^/@', $this->path) === 0)
				$this->path = '/'.$this->path;
			
			$this->query .= '';
			if(preg_match('/^(?:$|\\?)/', $this->query) === 0)
				$this->query = '?'.$this->query;
			
			$this->anchor .= '';
			if(!preg_match('/^(?:$|#)/', $this->anchor) === 0)
				$this->anchor = '#'.$this->anchor;
			
			$this->uri = $this->path . $this->query . $this->anchor;
			
			$this->user .= '';
			if($this->user == '')
				$this->user = null;
			
			if($this->password !== null)
				$this->password .= '';
			
			$this->server = strtolower($this->server);
			if(!preg_match('/^[a-z0-9][a-z0-9\\-]*(.[a-z0-9\\-]+)*$/', $this->server))
				$this->server = null;
			
			if($this->server === null)
				$this->local = '';
			else
			{
				$this->local = 'http'
					.($this->https ? 's' : '')
					.'://'
					.($this->user == '' ? '' : 
						$this->user
						.($this->password === null ? '' : ':'.$this->password)
						.'@')
					.$this->server
					.($this->port != ($this->https ? 463 : 80) ? ':'.$this->port : '')
				;
			}
				
			return $this->url = $this->local.$this->uri;
		}
	}
?>