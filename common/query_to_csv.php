<?php
	function query_result_to_csv(&$resultado, $quoted=false, $comma=null)
	{
		return queryResult2csv($resultado, $quoted);
	}
	
	
	
	function queryResult2csv(&$resultado, $quoted=false, $comma=';')
	{
		if(gettype($resultado) != 'array')
			return '';
		
		if(!is_string($comma) || strlen($comma) !== 1)
			$comma = ';';
		
		ob_start();
		
		for($registro = reset($resultado); key($resultado) !== null; $registro = next($resultado))
		{
			if(gettype($registro) != 'array')
				continue;
			
			if(count($registro) == 0)
				continue;
			
			
			if(@$registro[''] < 0)
				continue;
			
			
			$primeiro = true;
			
			for($valor = reset($registro); ($index = key($registro)) !==null; $valor = next($registro))
			{
				if($index === '')
					continue;
				
				if($primeiro)
					$primeiro = false;
				else
					echo $comma;
				
				if(!$quoted)
					echo str_replace($comma, 'COMMA', $valor);
				else
					echo '"'.str_replace('"', '""', $valor).'"';
			}
			
			echo "\n";
		}
		
		return ob_get_clean();
	}
?>