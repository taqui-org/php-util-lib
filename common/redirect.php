<?php
	function redirect($url, $append = '', $internal = false)
	{
		if(!is_string($url))
			$url = $_SERVER['REQUEST_URI'];
		
		$parsed = get_parsed_url($url);
		if($parsed === false)
		{
			$path = get_path_from_uri($url, $query, $anchor, true);
			
			if($path === false)
				apache_internal('Unable to parse the new location.');
			
			$parsed = @new ParsedURL();
			$parsed->path = $path;
			$parsed->query = $query;
			$parsed->anchor = $anchor;
		}
		
		$parsed->recalculate();
		
		if(!is_string($append))
			$append = '';
		
		$append_path = get_path_from_uri($append, $append_query, $append_anchor);
		
		$parsed->path .= $append_path;
		if($append_query !== '')
			$parsed->query;
		if($append_anchor !== '')
			$parsed->anchor;
		
		$parsed->recalculate();
		$redirect = $parsed->url;
		
		if(!$internal)
		{
			layout_clear();
			header('Location: '.$redirect);
			die();
		}
		else
			return $redirect;
	}
?>