<?php
	function calculate_base_path($file, $sub_dirs)
	{
		$file = dirname($file);
		
		for($i = 0; $i < $sub_dirs; $i++)
			$file = dirname($file);
		
		$file .= '/';
		
		$split_point = strlen($_SERVER['DOCUMENT_ROOT']);
		
		if(substr($file, 0, $split_point) == $_SERVER['DOCUMENT_ROOT'])
			return substr($file, $split_point);
		else
			return '';
	}
?>