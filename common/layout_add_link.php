<?php
	function layout_add_link($text, $address, $tip = '', $last = false)
	{
		$link = array('text' => $text, 'href' => $address, 'tip' => $tip);
		$links = &layout_links();
		
		if($last)
			$links[] = $link;
		else
			$links = array_merge(array($link), $links);
	}
?>