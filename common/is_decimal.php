<?php
	function is_decimal($number, $allow_negative = true)
	{
		return isDecimal($number, $allow_negative);
	}
	
	
	function isDecimal($number, $allow_negative = true)
	{
		switch(gettype($number))
		{
			case 'integer':
				return $allow_negative || ($number >= 0);
			
			case 'string':
				return preg_match('/^'.($allow_negative ? '-?' : '').'[0-9]+$/', $number) === 1;
			
			default:
				return false;
		}
	}
?>