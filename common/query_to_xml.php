<?php
	function query_result_xml(&$resultado, $type=false)
	{
		return queryResult2xml($resultado, $type);
	}
	
	
	
	function queryResult2xml(&$resultado, $type=false)
	{
		header('Content-Type: text/xml; charset=ISO-8859-1');
		
		echo '<?xml version="1.0" encoding="ISO-8859-1"?>'."\n";
		
		
		if(gettype($resultado) != 'array')
			return;
		
		
		echo "<Resultado>\n";
		
		for($registro = reset($resultado); key($resultado) !== null; $registro = next($resultado))
		{
			if(gettype($registro) != 'array')
				continue;
			
			if(count($registro) == 0)
				continue;
			
			
			if(@$registro[''] !== null && isValidXMLElementName($registro['']))
				$elemento = $registro[''];
			else
				$elemento = 'Registro';
			
			
			echo "\t<".$elemento.">\n";
			
			for($valor = reset($registro); ($index = key($registro)) !==null; $valor = next($registro))
			{
				if(!isValidXMLElementName($index))
					continue;
				
				echo "\t\t<".$index.($type === true ? ' type="'.gettype($valor).'"' : '').">";
				echo toXMLContent($valor);
				echo "</".$index.">\n";
			}
			
			echo "\t</".$elemento.">\n";
		}
		
		echo "</Resultado>\n";
		
		
		die();
	}
	
	
	
	function to_xml_content($text)
	{
		return toXMLContent($text);
	}
	
	
	
	function toXMLContent($text)
	{
		static $search = array('<', '&', '>');
		static $replacement = array('&lt;', '&amp;', '&gt;');
		
		return str_replace($search, $replacement, $text);
	}
	
	
	
	function is_valid_xml_element_name($name)
	{
		return isValidXMLElementName($name);
	}
	
	
	
	function isValidXMLElementName($name)
	{
		return (preg_match('/^(?!=xml|[0-9])[^ \t\n\r<:>="\']+$/', $name) === 1);
	}
?>