<?php
	function todbin($text, $add_delimiter = false, $conn = null)
	{
		return todbin($text, $add_delimiter, $conn);
	}
	
	
	
	function to_db_in($text, $add_delimiter = false, $conn = null)
	{
		if(!is_array($text))
			$text = array();
		
		return to_db($text, $add_delimiter, $conn);
	}
	
	
	
	function todb($text, $add_delimiter = false, $conn = null)
	{
		return to_db($text, $add_delimiter, $conn);
	}
	
	
	
	function to_db($text, $add_delimiter = false, $conn = null)
	{
		if($conn === null)
			$conn = config_get('db_link');
		
		if(!is_array($text))
			return to_mysql($text, $add_delimiter, $conn);
		
		$separator = '';
		$array = $text;
		$text = '';
		foreach($array as $current)
		{
			$text .= $separator;
			$text .= to_mysql($current, true, $conn);
			$separator = ',';
		}
		
		if($text == '')
			$text = 'null';
		
		if($add_delimiter)
			return '('.$text.')';
		else
			return $text;
	}
	
	
	
	function to_mysql($text, $add_delimiter = false, $conn = null, $is_in_utf8 = null)
	{
		if($add_delimiter && $text === null)
			return 'NULL';
		
		if($is_in_utf8 === null)
		{
			$is_in_utf8 = config_get('db-utf-8');
			if(!is_bool($is_in_utf8))
				$is_in_utf8 = !!config_get('utf-8');
		}
			
		if($is_in_utf8)
			$text = cp1252_decode($text, true);
		else
			$text = cp1252_encode($text, true);
		
		if($conn === null)
			$conn = config_get('db_link');
		
		$text = $conn->real_escape_string($text);
		
		if($add_delimiter)
		{
			switch($add_delimiter.'')
			{
				case '`':
					$text = str_replace('`', '``', $text);
				case '"':
				case "'":
					$delimiter = $add_delimiter;
					break;
				default:
					$delimiter = '"';
			}
				
			return $delimiter.$text.$delimiter;
		}
		else
			return $text;
	}
?>