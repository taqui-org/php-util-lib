<?php
	class HttpResponse
	{
		function HttpResponse()
		{
			$this->request_success = false;
			$this->request_error_msg = 'Not requested yet';
			
			$this->status_code = null;
			$this->status_msg = null;
			
			$this->headers = null;
			$this->body = null;
			$this->body_decoded = null;
			$this->decoded = null;
			
			$this->request_data = null;
			$this->request_url = null;
			
			$this->cookie = null;
			$this->content_type = null;
			$this->mime_type = null;
		}
		
		var $request_success;
		var $request_error_msg;
		var $request_data;
		var $request_url;
		
		var $cookie;
		var $content_type;
		var $mime_type;
		
		var $status_code;
		var $status_msg;
		
		var $headers;
		var $body;
		var $body_decoded;
		var $decoded;
	}
?>