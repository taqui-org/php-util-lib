<?php
	function layout_base_path($replacement = null)
	{
		static $base_path = '';
		
		if(is_string($replacement))
			$base_path = $replacement;
		
		return $base_path;
	}
?>