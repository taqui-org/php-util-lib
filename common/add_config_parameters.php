<?php
	function add_config_parameters($uri = null, $remove = null)
	{
		$uri = add_parameters(config_get('config_parameters'), $uri);
		
		if(is_string($remove))
			$remove = array($remove);
		
		if(is_array($remove))
			$uri = remove_parameters($remove, $uri);
		
		return $uri;
	}
?>