<?php
	function set_as_html($utf8 = null)
	{
		content_type('text/html', $utf8);
	}
	
	
	function set_as_xhtml($utf8 = null, $html_for_ie = true)
	{
		content_type('application/xhtml+xml', $utf8, $html_for_ie);
	}
	
	
	function set_as_xml($utf8 = null, $html_for_ie = false)
	{
		content_type('text/xml', $utf8, $html_for_ie);
	}
	
	
	function set_as_plain($utf8 = null)
	{
		content_type('text/plain', $utf8);
	}
	
	
	function content_type($type, $utf8 = null, $html_for_ie = true)
	{
		if(!is_bool($utf8))
			$utf8 = !!config_get('utf-8');
		
		if($utf8)
			$charset = 'utf-8';
		else
			$charset = 'iso-8859-1';
		
		if(($type === 'application/xhtml+xml' || $type === 'text/xml') && $html_for_ie && preg_match('/ MSIE /', $_SERVER['HTTP_USER_AGENT']))
			$type = 'text/html';
		
		header('Content-Type: '.$type.';charset='.$charset);
	}
?>