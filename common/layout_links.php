<?php
	function &layout_links($replacement = null)
	{
		static $links = array();
		
		if($replacement !== null && gettype($replacement) == 'array')
			$links = $replacement;
		
		return $links;
	}
?>