<?php
	function layout_title($replacement = null)
	{
		static $title = '';
		
		if($replacement !== null && gettype($replacement) == 'string')
			$title = $replacement;
		
		return $title;
	}
?>