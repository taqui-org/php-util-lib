<?php
	function &get_column(&$result, $column)
	{
		if(!is_array($result))
			return null;
		
		$values = array();
		foreach($result as $array)
		{
			if(!is_array($array))
				continue;
			
			$values[] = $array[$column];
		}
		
		return $values;
	}
?>