<?php
	function connect_db($host=null, $user=null, $password=null, $name=null, $utf8=null, $timezone=null)
	{
		if($host === null)
			$host = config_get('db_host');
		if($user === null)
			$user = config_get('db_user');
		if($password === null)
			$password = config_get('db_password');
		if($password === null)
			$password = config_get('db_pass');
		if($name === null)
			$name = config_get('db_name');
		
		global $db_host, $db_user, $db_password, $db_name;
		
		if($host === null)
			$host = $db_host;
		if($user === null)
			$user = $db_user;
		if($password === null)
			$password = $db_pass;
		if($name === null)
			$name = $db_name;
		
		try
		{
			$link = new mysqli($host, $user, $password, $name);
			if ($link->connect_error)
				layout_error_page("Could not connect:\n".$link->connect_error, true);
		}
		catch(Exception $e)
		{
			layout_error_page("Could not connect:\n".$e->getMessage(), true);
		}
		
		db_charset($utf8, $link);
		
		db_timezone($timezone, $link);
		
		$db_link = config_get('db_link');
		if($db_link === null)
			config_set('db_link', $link);
			
		return $link;
	}
	
	
	
	function db_change($name, $conn = null)
	{
		if($conn === null)
			$conn = config_get('db_link');
		
		$result = $conn->select_db($name);
		
		if(!$result)
			layout_error_page("Could not select database:\n".get_db_error($conn), true);
	}
	
	
	
	function db_tunning($conn = null)
	{
		$sql = 'SET SESSION group_concat_max_len = 2000000';
		execute_update($sql, $conn);
	}
	
	
	
	function db_charset($utf8, $conn = null)
	{
		if($utf8 === null)
			$utf8 = !!config_get('utf-8');
		
		config_set('db-uft-8', $utf8);
		
		if($utf8)
			$sql = 'SET NAMES "utf8" COLLATE "utf8_swedish_ci"';
		else
			$sql = 'SET NAMES "latin1" COLLATE "latin1_swedish_ci"';
		
		execute_update($sql, $conn);
	}
	
	
	
	function db_timezone($timezone, $conn = null)
	{
		if($timezone === null)
			$timezone = config_get('db_timezone');
		
		config_set('db_timezone', $timezone);
		
		if($timezone === null)
			$timezone = '@@global.time_zone';
		else
			$timezone = to_db($timezone, true, $conn);
		
		$sql = 'SET @@session.time_zone='.$timezone;
		execute_update($sql, $conn);
	}
	
	
	
	function get_db_error($conn = null)
	{
		if($conn === null)
			$conn = config_get('db_link');
		
		return $conn->error;
	}
?>