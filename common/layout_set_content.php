<?php
	function layout_set_content($file, $id = null, $eval = true, $headers = null)
	{
		$splitter = '/#(?:PAGE_CONTENTS|CONTEUDO_PAGINA)#/';
		
		if(preg_match($splitter, $file))
		{
			layout_content(preg_split($splitter, $file, 2), $id, $eval, $headers);
			return;
		}
		
		if(config_get('avoid_eval') === true)
			layout_content($file, $id, $eval);
		else
		{
			$layout = file_get_contents($file, true);
			if($layout === false)
				$layout = '';
			if(preg_match($splitter, $layout) === 0)
				$layout .= "\n#PAGE_CONTENTS#\n";
			
			layout_content(preg_split($splitter, $layout, 2), $id, $eval, $headers);
		}
	}
?>