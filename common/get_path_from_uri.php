<?php
	function get_path_from_uri($uri, &$query_string, &$anchor, $full = false)
	{
		if($uri === true)
			$uri = @$_SERVER['REQUEST_URI'];
		
		$parsed = get_parsed_url($uri);
		if($parsed === false)
		{
			if(preg_match('@^([^?#]*)(\\?[^#]*)?(#.*)?$@', $uri, $matches) !== 1)
				return false;
			
			$query_string = @$matches[2];
			$anchor = @$matches[3];
			$path = @$matches[1];
			
			if($full !== false)
			{
				$base = get_path($full);
				$base_dir = dirname($base);
				if($base === '' || $base[0] !== '/')
					return false;
				
				if($path === '')
					$path = basename($base);
				$base_dir = preg_replace('@(?<!/)$@', '/', $base_dir);
				$path = preg_replace('@^(?!/)@', $base_dir, $path);
			}
		}
		else
		{
			$query_string = $parsed->query;
			$anchor = $parsed->anchor;
			$path = $parsed->path;
		}
		
		return $path;
	}
?>