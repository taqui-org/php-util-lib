<?php
	function from_html($text, $new_line = false, $is_in_utf8 = null)
	{
		if($is_in_utf8 === null)
			$is_in_utf8 = config_get('utf-8');
		
		$text .= '';
		
		$text = trim(preg_replace("/(?<!\\s)[\\s]+(?!\\s)/", ' ', $text));
		
		$br = '@<br */?>@i';
		$text = preg_replace($br, "\n", $text);
		
		$text = str_replace(array('<![CDATA[', ']]>'), '', $text);
		
		$text = preg_replace('/<[^>]*>/', "\n", $text);
		
		$text = preg_replace_callback('/&#x([0-9a-z]+);/i', function($m){return pack("H*", $m[1]);}, html_entity_decode($text));
		$text = preg_replace_callback('/&#([0-9]{3});/', function($m){return pack("c", $m[1]);}, $text);
		
		if(!$new_line)
			$text = strtr($text, "\n", " ");
		
		$text = trim(preg_replace("/(?<![ \t\r])[ \t\r]+(?![ \t\r])/", ' ', $text));
		$text = trim(preg_replace("/(?<=\n) +(?! )/", "\n", $text));
		$text = trim(preg_replace("/(?<= ) +(?=\n)/", "", $text));
		
		if($is_in_utf8)
			$text = cp1252_decode($text, true);
		else
			$text = cp1252_encode($text, true);
		
		
		return trim($text);
	}
?>