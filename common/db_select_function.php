<?php
	function select_filter(&$result, $first, $last, $columns, $function)
	{
		if(!is_array($result))
			return;
			
		if(!function_exists($function))
			return;
		
		if(gettype($first) != 'integer')
			return;
		if(gettype($last) != 'integer')
			return;
		
		if(gettype($columns) != 'array' && $columns !== null)
			$columns = array($columns);
		
		$lines = count($result);
		
		if($first < 0)
			$first = $lines + $first;
		
		if($last <= 0)
			$last = $lines + $last - 1;
		
		$parameters = func_get_args();
		$parameters = array_slice($parameters, 4);
		
		for($i=$first; $i<=$last; $i++)
			foreach((is_array($columns) ? $columns : array_keys($result[$i])) as $column)
				$result[$i][$column] = call_user_func_array($function, array_merge(array($result[$i][$column]), $parameters));
	}
?>