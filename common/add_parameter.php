<?php
	function add_parameters($add, $uri = null)
	{
		$uri = replace_parameters($add, '', $has, $uri);
		$prefix = $has ? '&' : '?';
		
		if(is_array($add))
		{
			foreach($add as $parameter)
			{
				if(is_string($parameter))
				{
					$uri .= $prefix.$parameter;
					$prefix = '&';
				}
			}
		}
		
		return $uri;
	}
?>