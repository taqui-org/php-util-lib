<?php
	function add_to_config_parameters($value, $name = null)
	{
		if($name === null)
			$name = $value;
		
		config_add('config_parameters', $value, $name);
	}
?>