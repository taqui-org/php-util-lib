<?php
	function to_script($text, $new_line = true, $is_in_utf8 = null)
	{
		return toScript($text, $new_line, $is_in_utf8);
	}
	
	
	
	function toScript($text, $new_line = true, $is_in_utf8 = null)
	{
		static $search_nl = array("\n", "\r", "\t");
		static $replacement_nl = array('\n', '\r', '\t');
		
		if($is_in_utf8 === null)
			$is_in_utf8 = config_get('utf-8');
		
		$text .= '';
		
		if($is_in_utf8)
			$text = cp1252_decode($text, true);
		else
			$text = cp1252_encode($text, true);
		
		$text = addslashes($text);
		
		if($new_line)
			$text = str_replace($search_nl, $replacement_nl, $text);
		
		return $text;
	}
?>