<?php
	function get_parsed_url($url = true, $relative = false, $include_password = false)
	{
		if($url === true)
			$url = calculate_url($include_password);
		
		if(preg_match('%^http(s?)://(?:([^@:]*)(?::([^@]*))?@)?([^@/:]*)(?::([0-9]+))?(/[^#?]*)?(\\?[^#]*)?(#.*)?$%', $url, $matches) === 1)
			return new ParsedURL(@$matches[1], @$matches[4], @$matches[5], @$matches[6], @$matches[7], @$matches[8], @$matches[2], @$matches[3]);
		else
		{
			if($relative === true)
				$relative = calculate_url($include_password);
			
			if(is_string($relative))
			{
				$relative = get_parsed_url($relative);
				if($relative !== false)
				{
					$relative->path = get_path_from_uri($url, $query, $anchor, $relative->uri);
					$relative->query = $query;
					$relative->anchor = $anchor;
					
					$relative->recalculate();
					
					return $relative;
				}
			}
			  
			return false;
		}
	}
?>