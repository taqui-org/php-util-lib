<?php
	function static_content($content, $type, $date = null, $byDate = false, $attachment = false)
	{
		//Disables error reporting to avoid data corruption
		$previous_level = error_reporting(0);
		
		if($content !== null)
			layout_clear();
		
		if(!is_int($date))
		{
			$byDate = false;
			$date = null;
		}
		
		/* Warning: In this case the nothing will be done, but is usefull in some cases */
		if(!$byDate && $content === null)
			return;
		
		if($_SERVER['HTTP_IF_MATCH'] !== null || $_SERVER['HTTP_IF_NONE_MATCH'] !== null || !$byDate)
			$md5 = md5($content);
		
		if($_SERVER['HTTP_IF_UNMODIFIED_SINCE'] !== null || $_SERVER['HTTP_IF_MODIFIED_SINCE'] !== null || $byDate)
			$modification = $date;
		
		if(!$byDate)
			header('ETag: '.$md5);
		else
			header('Last-Modified: '.gmdate("D, d M Y H:i:s", $modification).' GMT');
		
		
		$code = null;
		
		do
		{
			if($_SERVER['HTTP_IF_MATCH'] !== null && $_SERVER['HTTP_IF_NONE_MATCH'] === null)
			{
				if($_SERVER['HTTP_IF_MATCH'] !== '*' && strtolower($_SERVER['HTTP_IF_MATCH']) !== $md5)
				{
					$code = 412;
					break;
				}
			}
			
			if($_SERVER['HTTP_IF_MATCH'] === null && $_SERVER['HTTP_IF_NONE_MATCH'] !== null)
			{
				if($_SERVER['HTTP_IF_MATCH'] === '*' || strtolower($_SERVER['HTTP_IF_NONE_MATCH']) === $md5)
					$code = 304;
				else
				{
					$code = 200;
					break;
				}
			}
			
			if($_SERVER['HTTP_IF_UNMODIFIED_SINCE'] !== null && $_SERVER['HTTP_IF_MODIFIED_SINCE'] === null)
			{
				if(strtotime($_SERVER['HTTP_IF_UNMODIFIED_SINCE']) === $modification)
				{
					$code = 412;
					break;
				}
			}
			
			if($_SERVER['HTTP_IF_UNMODIFIED_SINCE'] === null && $_SERVER['HTTP_IF_MODIFIED_SINCE'] !== null)
			{
				if(strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) !== $modification)
				{
					$code = 200;
					break;
				}
				else
					$code = 304;
			}
		}
		while(false);
		
		
		switch($code)
		{
			case 412:
				header('HTTP/1.1 412 Precondition Failed');
				header('Content-Lenght: 0');
				die();
			case 304:
				header('HTTP/1.1 304 Not Modified');
				header('Content-Lenght: 0');
				die();
			default:
				if($content !== null)
				{
					if(is_string($type))
						header('Content-Type: '.$type);
					header('Content-Lenght: '.strlen($content));
					
					if($attachment !== null && $attachment !== false)
					{
						if(is_string($attachment))
							$filename = ';filename="'.$attachment.'"';
						else
							$filename = '';
						
						header('Content-Disposition: attachment'.$filename);
					}
					
					echo $content;
					
					die();
				}
				else
				/* Warning: In this case the nothing might be done, but is usefull in some cases */
				{
					//Restores the previous error level
					error_reporting($previous_level);
				}
		}
	}
?>