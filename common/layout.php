<?php
	function layout($title = null, $id = null)
	{
		@ob_end_clean();
		ob_start();
		
		if($id === null && !is_string($title))
		{
			$id = $title;
			$title = null;
		}
		
		layout_title($title);
		layout_header($id);
		register_shutdown_function('layout_footer', $id);
	}
?>