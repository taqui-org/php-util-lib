<?php
	function &select_one_row($sql, $conn=null, $assoc = true)
	{
		return selectOneRow($sql, $conn, $assoc);
	}
	
	
	
	function &selectOneRow($sql, $conn=null, $assoc = true)
	{
		checkInvertedSelectParams($conn, $assoc);
		
		$result = executeSelect($sql, $conn);
		$nRows = $result['rows'];
		$result = $result['result'];
		
		if($nRows > 1)
			layout_error_page("Too many rows returned when only one were expected: ".$sql, true);
		
		if($nRows == 0)
			return null;
		
		$row = $result->fetch_array($assoc ? MYSQLI_ASSOC : MYSQLI_NUM);
		
		if($row === false)
			layout_error_page("Error while reading the rows returned by: ".$sql);
		
		return $row;
	}
?>