<?php
	function execute_insert($sql, $conn=null)
	{
		return executeInsert($sql, $conn);
	}
	
	
	
	function executeInsert($sql, $conn=null)
	{
		if($conn === null)
			$conn = config_get('db_link');
		
		executeUpdate($sql, $conn);
		
		return $conn->insert_id;
	}
?>