<?php
	function to_html($text, $new_line = false, $is_in_utf8 = null, $all_chars = false)
	{
		return toHTML($text, $new_line, $is_in_utf8, $all_chars);
	}
	
	
	
	function toHTML($text, $new_line = false, $is_in_utf8 = null, $all_chars = false)
	{
		static $search_nl = array("/(\r\n|(?<!\r)\n|\r(?!\n))/", "/(?<=\t|\n|\r)\t/", '/^ /', '/ $/', '/  /');
		static $replacement_nl_iso = array('<br>$1', '    ', "\xA0", "\xA0", " \xA0");
		static $replacement_nl_utf = array('<br>$1', '    ', "\xC2\xA0", "\xC2\xA0", " \xC2\xA0");
		
		static $search = null;
		static $replacement_iso = null;
		static $replacement_utf = null;
		
		if($search === null)
		{
			$search = array_slice($search_nl, 1);
			$replacement_iso = array_slice($replacement_nl_iso, 1);
			$replacement_utf = array_slice($replacement_nl_utf, 1);
		}
		
		if($is_in_utf8 === null)
			$is_in_utf8 = config_get('utf-8');
		
		$text .= '';
		
		if($is_in_utf8)
			$text = cp1252_decode($text, true);
		else
			$text = cp1252_encode($text, true);
		
		if($all_chars)
			$text = htmlentities($text);
		else
			$text = htmlspecialchars($text);
		
		if($new_line)
			$text = preg_replace($search_nl, $is_in_utf8 ? $replacement_nl_utf : $replacement_nl_iso, $text);
		else
			$text = preg_replace($search, $is_in_utf8 ? $replacement_utf : $replacement_iso, $text);
		
		return $text;
	}
?>