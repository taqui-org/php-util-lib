<?php
	function convert_charset($text, $input, $output)
	{
		return convertCharset($text, $input, $output);
	}
	
	
	
	function convertCharset($text, $input, $output)
	{
		static $function_name=array();
		
		if(!is_string($input) || trim($input)==='' || !is_string($output) || trim($output)==='')
			return false;
		
		if(!isset($function_name[$input][$output]))
		{
			$defined=false;
			
			if($input===$output)
			{
				$function_name[$input][$output]='';
				$defined=true;
			}
			
			
			if(!$defined && function_exists('utf8_encode') && function_exists('utf8_decode'))
			{
				if(strtoupper($input)==='ISO-8859-1' && strtoupper($output)==='UTF-8')
				{
					$function_name[$input][$output]='utf8_encode';
					$defined=true;
				}
				
				if(strtoupper($input)==='UTF-8' && strtoupper($output)==='ISO-8859-1')
				{
					$function_name[$input][$output]='utf8_decode';
					$defined=true;
				}
			}
			
			
			if(!$defined && function_exists('mb_convert_encoding'))
			{
				if(@mb_convert_encoding('', $input, $input)!==false)
					if(@mb_convert_encoding('', $output, $output)!==false)
					{
						$function_name[$input][$output]='mb_convert_encoding';
						$defined=true;
					}
			}
			
			
			if(!$defined && function_exists('iconv'))
			{
				if(@iconv($input, $output, '')!==false)
				{
					$function_name[$input][$output]='iconv';
						$defined=true;
				}
			}
			
			
			if(!$defined && function_exists('cp1252_encode') && function_exists('cp1252_decode'))
			{
				if(strtoupper($input)==='CP1252' && strtoupper($output)==='UTF-8')
				{
					$function_name[$input][$output]='cp1252_decode';
					$defined=true;
				}
				
				if(strtoupper($input)==='UTF-8' && strtoupper($output)==='CP1252')
				{
					$function_name[$input][$output]='cp1252_encode';
					$defined=true;
				}
			}
			
			
			if(!$defined)
				$function_name[$input][$output]=false;
		}
		
		
		if(is_array($text))
		{
			foreach($text as $key => $value)
				$text[$key]=convertCharset($value, $input, $output);
			
			return $text;
		}
		
		
		switch($function_name[$input][$output])
		{
			case 'mb_convert_encoding':
				return mb_convert_encoding($text, $output, $input);
			case 'iconv':
				return iconv($input, $output, $text);
			case 'utf8_encode':
				return utf8_encode($text);
			case 'utf8_decode':
				return utf8_decode($text);
			case 'cp1252_encode':
				return cp1252_encode($text);
			case 'cp1252_decode':
				return cp1252_decode($text);
			case '':
				return $text;
			default:
				return false;
		}
	}
?>