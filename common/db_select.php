<?php
	function execute_select($sql, $conn=null)
	{
		return executeSelect($sql, $conn);
	}
	
	
	
	function executeSelect($sql, $conn=null)
	{
		if($conn === null)
			$conn = config_get('db_link');
		
		$result = $conn->query($sql);
		
		if(preg_match("/^(\n|\r\n|\r)/", $sql) !== 1)
			$sql = "\n".$sql;
		
		if($result === false)
			layout_error_page("Could not execute the query: ".$sql."\n".get_db_error($conn), true);
		
		$nRows = $result->num_rows;
		
		if($nRows === false)
			layout_error_page("Could not retrieve the number of rows returned: ".$sql."\n".get_db_error($conn), true);
		
		return array('result' => $result, 'rows' => $nRows, 0 => $result, 1 => $nRows);
	}
	
	
	
	function checkInvertedSelectParams(&$conn, &$assoc)
	{
		if($conn instanceof MySQLi)
			return;
		
		if($assoc instanceof MySQLi_Result || is_bool($conn))
		{
			$temp = $conn;
			$conn = $assoc;
			$assoc = $temp;
		}
	}
?>