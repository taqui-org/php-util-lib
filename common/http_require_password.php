<?php
	function require_password()
	{
		require_password_from(config_get('http_users'), config_get('http_users_ip'), config_get('http_realm'));
	}
	
	
	
	function http_realm_set($realm)
	{
		config_set('http_realm', $realm);
	}
	
	
	
	function http_user_set($user, $password_md5)
	{
		if($user !== null)
			config_add('http_users', $password_md5, $user);
	}
	
	
	
	function http_user_host_set($user, $host)
	{
		if($user !== null)
			config_add('http_users_ip', $host, $user);
	}
?>