<?php
	function isDateOrTime($date, &$dateFormatted, $type = false, $time = null)
	{
		$dateFormatted = null;
		
		if($time === false)
			$reTime = '$';
		else if($time === true)
			$reTime = '[^0-9]([0-9]{2})[^0-9]([0-9]{2})[^0-9]([0-9]{2})(?:[^0-9][0-9])?$';
		else
			$reTime = '(?:[^0-9]([0-9]{2})[^0-9]([0-9]{2})(?:[^0-9]([0-9]{2})(?:[^0-9]|$)|$)|$)';
		
		$reDate1 = '^([0-9]{4})[^0-9]([0-9]{2})[^0-9]([0-9]{2})'.$reTime;
		$reDate2 = '^([0-9]{2})[^0-9]([0-9]{2})[^0-9]([0-9]{4})'.$reTime;
		$reDate3 = '^((?:20|19)[0-9]{2})([0-9]{2})([0-9]{2})';
		if($reTime != '$')
			$reTime = '([0-9]{2})([0-9]{2})([0-9]{2})$';
		$reDate3 .= $reTime;
		
		if(preg_match('@'.$reDate1.'@', $date, $matches) !== 1 && preg_match('@'.$reDate3.'@', $date, $matches) !== 1)
		{
			if(preg_match('@'.$reDate2.'@', $date, $matches) !== 1)
				return false;
			else
			{
				$year = $matches[3];
				$day = $matches[1];
			}
		}
		else
		{
			$year = $matches[1];
			$day = $matches[3];
		}
		
		$month = $matches[2];
		$hour = (@$matches[4] == null ? '00' : $matches[4]);
		$minute = (@$matches[5] == null ? '00' : $matches[5]);
		$second = (@$matches[6] == null ? '00' : $matches[6]);
		
		if(gmdate('YmdHis', gmmktime($hour, $minute, $second, $month, $day, $year)) === $year.$month.$day.$hour.$minute.$second)
		{
			$time = '';
			
			if($time !== false || !is_bool($type))
			{
				if($time === true || @$matches[4] != null || !is_bool($type))
					$time = ' '.$hour.':'.$minute.':'.$second;
			}
			
			if($type === true)
				$dateFormatted = $day.'/'.$month.'/'.$year.$time;
			else
			{
				$dateFormatted = $year.'-'.$month.'-'.$day.$time;
			
				if($type !== false) /* 4J */
					$dateFormatted .= '.0';
			}
			
			return true;
		}
		else
			return false;
	}
?>