<?php
	function &xml_parser(&$xml, $accept_header=true)
	{
		return XMLParser::xml_parser($xml, $accept_header);
	}
	
	
	
	class XMLParser
	{
		function xml_error_msg($xml, $source)
		{
			return '['.$source.'] XML parse error near: '.substr($xml, 0, 50);
		}
		
		
		
		function &xml_parser(&$xml, $accept_header=true, $level=1)
		{
			$level = (int) $level;
			
			if($level < 1)
				$level = 1;
			
			if($accept_header === true)
			{
				if(preg_match('/^[\n\r\t ]*<\\?xml [^?<>]*\\?>/i', $xml, $match) === 1)
					$xml = substr($xml, strlen($match[0]));
				
				if(preg_match('/^[\n\r\t ]*<!DOCTYPE [^?<>]*\\?>/i', $xml, $match) === 1)
					$xml = substr($xml, strlen($match[0]));
			}
			
			$result = array();
			
			while(true)
			{
				if(preg_match('/^[\n\r\t ]*<([a-z_]+)>/i', $xml, $head) === 1)
				{
					$xml = substr($xml, strlen($head[0]));
					$item = array();
					$item[':'] = true;
					$item[':name'] = $head[1];
					
					if(preg_match('/^([^<>]*)<\\/'.$head[1].'>/', $xml, $tail) !== 1)
					{
						$item[':data'] = XMLParser::xml_parser($xml, false, $level+1);
						
						if(gettype($item[':data']) == 'string')
							return $item[':data'];
						
						if(preg_match('/^[\n\r\t ]*<\\/'.$head[1].'>/', $xml, $tail) === 1)
							$xml = substr($xml, strlen($tail[0]));
						else
							return XMLParser::xml_error_msg($xml, 'Incorrect end tag');
					}
					else
					{
						$item[':data'] = $tail[1];
						$xml = substr($xml, strlen($tail[0]));
					}
					
					$result[] = $item;
				}
				else
					break;
			}
			
			if($level == 1)
			{
				if(preg_match('/^[\n\r\t ]*$/', $xml) !== 1)
					return XMLParser::xml_error_msg($xml, 'Invalid or unsupported XML');
				
				if(count($result) == 0)
					return XMLParser::xml_error_msg($xml, 'Empty XML');
			}
			
			$counts = array();
			foreach($result as $item)
				$counts[$item[':name']]++;
			
			$largest = 0;
			foreach($counts as $name => $num)
				$largest = max($largest, $num);
			
			if($largest <= 1)
			{
				$map = array();
				foreach($result as $item)
					$map[$item[':name']] = $item[':data'];
				
				return $map;
			}
			else
			{
				if(count($counts) > 1)
					return $result;
				
				$vector = array();
				foreach($result as $item)
					$vector[] = $item[':data'];
				
				return $vector;
			}
		}
	}
?>