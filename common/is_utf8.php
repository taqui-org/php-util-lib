<?php
	function is_utf8($text)
	{
		$text .= '';
		
		while($text != '')
		{
			$subject = substr($text, 0, 5120);
			if(!preg_match('@^(?:[\\x00-\\x7f]|[\\xc2-\\xdf][\\x80-\\xbf]|\\xe0[\\xa0-\\xbf][\\x80-\\xbf]|[\\xe1-\\xec][\\x80-\\xbf]{2}|\\xed[\\x80-\\x9f][\\x80-\\xbf]|[\\xee\\xef][\\x80-\\xbf]{2}|\\xf0[\\x90-\\xbf][\\x80-\\xbf]{2}|[\\xf1-\\xf3][\\x80-\\xbf]{3}|\\xf4[\\x80-\\x8f][\\x80-\\xbf]{2})*$@', $subject, $match))
				return false;
			
			$text = substr($text, strlen($match[0]));
		}
		
		return true;
	}
?>