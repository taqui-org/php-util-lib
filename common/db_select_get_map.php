<?php
	function &get_map(&$result, $keys = null, $values = null)
	{
		if(!is_array($result))
			return null;
		
		$map = array();
		foreach($result as $key => $array)
		{
			if(!is_array($array))
				continue;
			
			$map[$keys !== null ? $array[$keys] : $key] = $values !== null ? $array[$values] : $array;
		}
		
		return $map;
	}
?>