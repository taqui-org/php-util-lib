<?php
	function &select_first_column($sql, $assoc = true, $conn=null)
	{
		return selectFirstColumn($sql, $assoc, $conn);
	}
	
	
	
	function &selectFirstColumn($sql, $assoc = true, $conn=null)
	{
		checkInvertedSelectParams($conn, $assoc);
		
		$result = executeSelect($sql, $conn);
		$nRows = $result['rows'];
		$result = $result['result'];
		
		$values = array();
		
		while($row = $result->fetch_array($assoc ? MYSQLI_ASSOC : MYSQLI_NUM))
			$values[] = reset($row);
		
		return $values;
	}
?>