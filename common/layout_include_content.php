<?php
	function layout_include_content($index, $id = null)
	{
		$eval = false;
		$content = layout_content(null, $id);
		$headers = null;
		if(is_array($content))
		{
			$eval = $content['eval'];
			$headers = $content['headers'];
			$content = $content['content'];
		}
		
		if(is_array($content))
		{
			if(isset($content[$index]))
				$content = $content[$index];
			else
			{
				$index = 0;
				$content = reset($content);
			}
			
			if($index == 0 && !headers_sent())
			{
				if(is_string($headers))
					$headers = array($headers);
					
				if(is_array($headers))
				{
					foreach($headers as $header => $value)
					{
						if(is_string($header))
							header($header.': '.$value);
						else
							header($value);
					}
				}
			}
			
			if($eval)
				eval('?>'.$content);
			else
				echo $content;
		}
		else
		{
			if(is_string($content))
			{
				if($eval)
					include($content);
				else
					echo file_get_contents($content, true);
			}
		}
	}
?>