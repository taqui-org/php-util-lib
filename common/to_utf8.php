<?php
	function to_utf8( $text, $filterChars = false )
	{
		return toUtf8( $text, $filterChars );
	}
	
	
	function toUtf8( $text, $filterChars = false )
	{
		if( $text === "\n\t" )
			$text = '';
		
		if( isUCS( $text ) )
		{
			$text = preg_replace( '@\\x00(?=\\x00|$)|\\x00\\xC2\\x9D@', "\xFF\xFD", $text );
			$text = iconv( 'UCS-2BE', 'UTF-8', $text );
		}
		else if( isGSM( $text ) )
			$text = gsmToUtf( $text );
		else if( !isUTF( $text ) )
			$text = iconv( 'cp1252', 'UTF-8//TRANSLIT', $text );
		
		$text = iconv( 'UTF-8', 'UTF-8//TRANSLIT', $text );
		
		if( $filterChars )
			$text = preg_replace( array('@[\\x00-\\x08\\x0B\\x0C\\x0E-\\x1F]@','@[\\x09\\x0A\\x0D]+@'), array("\xFF\xFD", ' '), $text );
		
		return $text;
	}
	
	
	function isGSM( $text )
	{
		if( preg_match('@[\\x80-\\xFF]@', $text ) )
			return false;

		if( preg_match('@^[\\x20-\\x7F]*$@', $text ) )
			return false;
		
		return true;
	}
	
	
	function isUCS( $text, $count = null )
	{
		if( !is_array( $count ) )
			$count = count_chars( $text );
		
		return ( $count[0] >= ( strlen( $text ) * 0.4 ) && $count[0] <= ( strlen( $text ) * 0.7 ) && $count[0] > 0 ); 
	}
	
	
	function isUTF( $text, $count = null )
	{
		return preg_match('@^(?:[\\x00-\\x7f]|[\\xc2-\\xdf][\\x80-\\xbf]|\\xe0[\\xa0-\\xbf][\\x80-\\xbf]|[\\xe1-\\xec][\\x80-\\xbf]{2}|\\xed[\\x80-\\x9f][\\x80-\\xbf]|[\\xee\\xef][\\x80-\\xbf]{2}|\\xf0[\\x90-\\xbf][\\x80-\\xbf]{2}|[\\xf1-\\xf3][\\x80-\\xbf]{3}|\\xf4[\\x80-\\x8f][\\x80-\\xbf]{2})*$@', $text );
	}
?>