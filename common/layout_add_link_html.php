<?php
	function layout_add_link_html($html, $last = false)
	{
		$links = &layout_links();
		
		if($last)
			$links[] = $html;
		else
			$links = array_merge(array($html), $links);
	}
?>