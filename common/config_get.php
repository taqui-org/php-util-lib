<?php
	function config_get($name, $index = null)
	{
		$temp = config_base($name, null, false);
		if(is_array($temp) && $index !== null)
			return @$temp[$index];
		else
			return $temp;
	}
?>