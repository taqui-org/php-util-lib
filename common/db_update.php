<?php
	function execute_update($sql, $conn=null, $affected = false)
	{
		return executeUpdate($sql, $conn, $affected);
	}
	
	
	
	function executeUpdate($sql, $conn=null, $affected = false)
	{
		checkInvertedSelectParams($conn, $affected);
		
		if($conn === null)
			$conn = config_get('db_link');
		
		$status = $conn->query($sql);
		
		if(preg_match("/^(\n|\r\n|\r)/", $sql) !== 1)
			$sql = "\n".$sql;
		
		if(!$status)
			layout_error_page("Could not execute the sql: ".$sql."\n".get_db_error($conn), true);
		
		for($go=true; $go; $affected=true)
		{
			if($affected)
			{
				$nRows = $conn->affected_rows;
				break;
			}
			else
			{
				$info = $conn->info;
				
				if($info)
				{
					$nRows = (int) preg_replace('/^[^0-9]*([0-9]+)[^0-9].*$/', '\\1', $info);
					break;
				}
				else
					continue;
			}
		}
		
		if($nRows === false)
			layout_error_page("Could not retrieve the number of affected rows: ".$sql."\n".get_db_error($conn), true);
		
		return $nRows;
	}
?>