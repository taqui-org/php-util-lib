<?php
	function replace_parameters($remove, $replacement, &$has_query_string, $uri = null)
	{
		if(!is_string($replacement))
			$replacement = '';
		$replacement = trim($replacement);
		if($replacement !== '')
			 $replacement = '\\1'.$replacement;
		
		if(!is_string($uri))
			$uri = $_SERVER['REQUEST_URI'];
		
		if(!is_array($remove))
		{
			if(is_string($remove))
				$remove = array($remove);
			else
				$remove = array();
		}
		
		$path = get_path_from_uri($uri, $query_string, $anchor);
		
		if($query_string != '')
		{
			foreach($remove as $parameter)
				$query_string = preg_replace('@([?&])'.preg_quote(urlencode($parameter)).'(=[^&]*)?(?=&|$)@i', $replacement, $query_string);
			
			if(strlen($query_string) >= 1 && $query_string[0] == '&')
				$query_string[0] = '?';
		}
		
		$has_query_string = $query_string != '';
		
		return $path.$query_string.$anchor;
	}
?>