<?php
	function &http_submit($servidor, $caminho, $dados=null, $porta=80, $post=true, $connection_timeout=10, $socket_timeout = 20, $https=false, $headers=null, $redirect=false)
	{
		return httpSubmit($servidor, $caminho, $dados, $porta, $post, $connection_timeout, $socket_timeout, $https, $headers, $redirect);
	}
	
	
	function &httpSubmit($servidor, $caminho, $dados=null, $porta=80, $post=true, $connection_timeout=10, $socket_timeout = 20, $https=false, $headers=null, $redirect=false)
	{
		$resposta = new HttpResponse();
		
		$msg = '';
		
		if(gettype($servidor) !== 'string')
			$msg .= "Invalid server name: $servidor\n";
		
		if(gettype($caminho) !== 'string')
			$msg .= "Invalid path: $caminho\n";
		
		if($dados === null)
			$dados = array();
		
		if(gettype($porta) !== 'integer' || $porta <= 0)
			$msg .= "Invalid server port: $porta\n";
		
		if(gettype($connection_timeout) !== 'integer' || $connection_timeout <= 0)
			$msg .= "Invalid connection timeout: $connection_timeout\n";
		
		if(gettype($socket_timeout) !== 'integer' || $socket_timeout <= 0)
			$msg .= "Invalid socket timeout: $socket_timeout\n";
		
		if($headers === null)
			$headers = array();
		
		if(gettype($headers) !== 'array')
			$msg .= "Invalid request HTTP headers!\n";
		
		$msg = trim($msg);
		
		if($msg != '')
		{
			$resposta->request_success = false;
			$resposta->request_error_msg = $msg;
			
			return $resposta;
		}
		
		
		$requisicao = '';
		
		if(gettype($dados) == 'array')
			$dados = arrayToQueryString($dados);
		else
			$dados .= '';
		
		if(!$post)
		{
			if($dados !== '')
			{
				$caminho = get_path_from_uri($caminho, $query_string, $anchor);
				$caminho .= $query_string;
				
				if($query_string == '')
					$caminho .= '?';
				else
					$caminho .= '&';
				
				$caminho .= $dados.$anchor;
				
				$dados = '';
			}
			
			$requisicao .= 'GET '.$caminho.' HTTP/1.0'."\r\n";
		}
		else
			$requisicao .= 'POST '.$caminho.' HTTP/1.0'."\r\n";
		
		$url = 'http'.($https ? 's' : '').'://'.$servidor.($porta == 80 && !$https || $porta == 443 && $https ? '' : ':'.$porta).$caminho;
		$resposta->request_url = $url;
		
		$requisicao .= 'Host: '.(@$headers['Host'] !== null ? $headers['Host'] : $servidor)."\r\n";
		$requisicao .= 'Connection: Close'."\r\n";
		
		if($post)
		{
			if(@$headers['Content-Type'] === null)
				$requisicao .= 'Content-Type: application/x-www-form-urlencoded'."\r\n";
			
			$requisicao .= 'Content-Length: '.strlen($dados)."\r\n";
		}
		
		foreach($headers as $nome => $lista)
		{
			if(preg_match('/['.(is_int($nome) ? '' : ':').'\r\n]/', $nome) === 1)
			{
				$resposta->request_success = false;
				$resposta->request_error_msg = 'Invalid HTTP header: '.$nome;
				
				return $resposta;
			}
			
			if(!is_array($lista))
				$lista = array($lista);
			
			foreach($lista as $valor)
			{
				if(preg_match('/[\r\n]/', $valor) === 1)
				{
					$resposta->request_success = false;
					$resposta->request_error_msg = 'Invalid HTTP header: '.$nome.': '.$valor;
					
					return $resposta;
				}
				
				$linha = (is_int($nome) ? '' : $nome.': ').$valor."\r\n";
				if(preg_match('/^(?:Host|Connection):/i', $linha) !== 1)
					$requisicao .= $linha;
			}
		}
		
		$requisicao .= "\r\n";
		
		if($post)
			$requisicao .= $dados;
		
		
		if($https === true)
			$https = 'ssl://';
		else
			$https = '';
		
		
		$ref = @fsockopen($https.$servidor, $porta, $erro_num, $erro_msg, $connection_timeout);
		
		if($ref === false)
		{
			$resposta->request_success = false;
			$resposta->request_error_msg = 'Connection error to '.$https.$servidor.':'.$porta.': '.$erro_num.' - '.$erro_msg;
			
			return $resposta;
		}
		
		if(@stream_set_timeout($ref, $socket_timeout) === false)
		{
			$resposta->request_success = false;
			$resposta->request_error_msg = 'Socket error to '.$https.$servidor.':'.$porta.': '.$erro_num.' - '.$erro_msg;
			
			return $resposta;
		}
		
		$resposta->request_data = $requisicao;
		
		if(@fwrite($ref, $requisicao) === false)
		{
			$resposta->request_success = false;
			$resposta->request_error_msg = 'Data submit error to '.$url;
			
			return $resposta;
		}
		
		
		$resposta->headers = array();
		$primeira = true;
		
		while(!@feof($ref) && ($linha = @fgets($ref)) !== false)
		{
			if(substr($linha, -2) !== "\r\n")
			{
				$resposta->request_success = false;
				$resposta->request_error_msg = $servidor.' sent a response with a invalid header: '.$linha;
				
				return $resposta;
			}
			
			if($linha === "\r\n")
			{
				if($primeira)
				{
					$resposta->request_success = false;
					$resposta->request_error_msg = $servidor.' sent a response a empty header!';
					
					return $resposta;
				}
				else
					break;
			}
			
			if($primeira)
			{
				$primeira = false;
				
				if(preg_match('/HTTP\\/1\\.[0-1] ([0-9]{3})([^\r\n]*)\r\n$/', $linha, $matches) !== 1)
				{
					$resposta->request_success = false;
					$resposta->request_error_msg = $servidor.' sent a response with a invalid header: '.$linha;
					
					return $resposta;
				}
				
				$resposta->status_code = $matches[1] + 0;
				$resposta->status_msg = trim(@$matches[2]);
			}
			else
			{
				if(preg_match('/^([a-z0-9\\-]+): ([^\r\n]*)\r\n$/i', $linha, $matches) !== 1)
				{
					$resposta->request_success = false;
					$resposta->request_error_msg = $servidor.' sent a response with a invalid header: '.$linha;
					
					return $resposta;
				}
				
				$matches[1] = preg_replace_callback('/((?:^|-)[a-z])/', function($m){return strtoupper($m[1]);}, $matches[1]);
				
				if(@$resposta->headers[$matches[1]] === null)
					$resposta->headers[$matches[1]] = $matches[2];
				else
				{
					if(gettype($resposta->headers[$matches[1]]) !== 'array')
					{
						if($resposta->headers[$matches[1]] !== $matches[2])
							$resposta->headers[$matches[1]] = array($resposta->headers[$matches[1]], $matches[2]);
					}
					else
					{
						if(array_search($matches[2], $resposta->headers[$matches[1]]) === false)
							$resposta->headers[$matches[1]][] = $matches[2];
					}
				}
			}
		}
		
		
		$resposta->content_type = @$resposta->headers['Content-Type'];
		if(preg_match('@^([a-z0-9\-_+]+/[a-z0-9\-_+]+)(?:$|;)@i', @$resposta->headers['Content-Type'], $matches))
			$resposta->mime_type = $matches[1];
		
		$cookies = array();
		foreach(array(@$headers['Cookie'], @$resposta->headers['Set-Cookie']) as $array)
		{
			if(!is_array($array))
				$array = array($array);

			foreach($array as $current)
			{
				if(is_string($current))
				{
					foreach(explode(';', $current) as $current)
					{
						$pair = explode('=', $current, 2);
						$name = trim($pair[0]);
						if(count($pair) > 1 && !preg_match('/^(Path|Domain|Expires|HTTPOnly)$/i', $name))
							$cookies[$name] = trim($pair[1]);
					}
				}
			}
		}
		
		if(count($cookies) > 0)
		{
			$separator = '';
			$resposta->cookie = '';
			foreach($cookies as $name => $value)
			{
				$resposta->cookie .= $separator.$name.'='.$value;
				$separator = ';';
			}
		}
				
		
		if(@$resposta->headers['Content-Length'] !== null && preg_match('/[0-9]+/', $resposta->headers['Content-Length']) === 1)
			$tamanho = (int) $resposta->headers['Content-Length'];
		else
			$tamanho = 2147483647;
		
		$ler = 65536;
		$resposta->body = '';
		
		while(!@feof($ref) && strlen($resposta->body) < $tamanho)
		{
			$parte = @fread($ref, $ler);
			
			if($parte === false)
			{
				$resposta->request_success = false;
				$resposta->request_error_msg = 'Error while receiving the content from: '.$url;
				
				return $resposta;
			}
			
			$resposta->body .= $parte;
		}
		
		@fclose($ref);
		
		
		$resposta->request_success = true;
		$resposta->request_error_msg = null;
		
		
		if($redirect!==false && ($resposta->status_code == 301 || $resposta->status_code == 302 || $resposta->status_code == 303))
		{
			if(!is_int($redirect))
				$redirect = 1;
			
			if($redirect <= 5)
			{
				$new_url = get_parsed_url(@$resposta->headers['Location'], $url);
				
				if(is_array($new_url))
					$new_url = end($new_url);
				
				if($new_url)
				{
					$redirect++;
					
					$https = $new_url->https;
					$servidor = $new_url->server;
					$porta = $new_url->port;
					$caminho = $new_url->uri;
					
					if($resposta->cookie !== null)
						$headers['Cookie'] = $resposta->cookie;
					
					return httpSubmit($servidor, $caminho, null, $porta, false, $connection_timeout, $socket_timeout, $https, $headers, $redirect);
				}
			}
		}
		
		switch(@$resposta->headers['Content-Encoding'])
		{
			case 'gzip':
				$resposta->body_decoded  = @gzinflate(substr($resposta->body, 10));//gzdecode
				$resposta->decoded = true;
				break;
				
			case 'deflate':
				$resposta->body_decoded  = @gzinflate($resposta->body);
				$resposta->decoded = true;
				break;
				
			default:
				$resposta->body_decoded  = &$resposta->body;
				$resposta->decoded = false;
		}
		
		return $resposta;
	}
?>