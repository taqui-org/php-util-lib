<?php
	function no_break($text, $new_line = true, $is_in_utf8 = null)
	{
		return noBreak($text, $new_line, $is_in_utf8);
	}
	
	
	
	function noBreak($text, $new_line = true, $is_in_utf8 = null)
	{
		static $search_nl = "/(?:\r\n|(?<!\r)\n|\r(?!\n)| )/";
		static $search = "/ /";
		static $replacement_iso = "\xA0";
		static $replacement_utf = "\xC2\xA0";
		
		if($is_in_utf8 === null)
			$is_in_utf8 = config_get('utf-8');
		
		$text .= '';
		$text = preg_replace($new_line ? $search_nl : $search, $is_in_utf8 ? $replacement_utf : $replacement_iso, $text);
		return $text;
	}
?>