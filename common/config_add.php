<?php
	function config_add($name, $value, $key = null)
	{
		$config = config_base($name, null, false);
		
		if(!is_array($config))
			$config = array();
		
		if($key === null)
			$config[] = $value;
		else
			$config[$key] = $value;
		
		config_set($name, $config);
	}
?>