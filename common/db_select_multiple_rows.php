<?php
	function &select_multiple_rows($sql, $conn=null, $assoc = true)
	{
		return selectMultipleRows($sql, $conn, $assoc);
	}
	
	
	
	function &selectMultipleRows($sql, $conn=null, $assoc = true)
	{
		checkInvertedSelectParams($conn, $assoc);
		
		$result = executeSelect($sql, $conn);
		$nRows = $result['rows'];
		$result = $result['result'];
		
		$rows = array();
		
		while($row = $result->fetch_array($assoc ? MYSQLI_ASSOC : MYSQLI_NUM))
			$rows[] = $row;
			
		return $rows;
	}
?>