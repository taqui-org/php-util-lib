<?php
	function sum_select_result(&$result, $first, $last, $destiny, $columns)
	{
		sumSelectResult($result, $first, $last, $destiny, $columns);
	}
	
	
	
	function sumSelectResult(&$result, $first, $last, $destiny, $columns)
	{
		if(gettype($result) != 'array')
			return;
		
		if(gettype($first) != 'integer')
			return;
		if(gettype($last) != 'integer')
			return;
		if(gettype($destiny) != 'integer')
			return;
		
		if(gettype($columns) != 'array')
			$columns = array($columns);
		
		$lines = count($result);
		
		if($first < 0)
			$first = $lines + $first;
		
		if($last <= 0)
			$last = $lines + $last - 1;
		
		if($destiny <= 0)
			$destiny = $lines + $destiny;
		
		for($i=$first; $i<=$last; $i++)
			foreach($columns as $column)
				$sum[$column] += $result[$i][$column];
		
		foreach($columns as $column)
			$result[$destiny][$column] = $sum[$column] + 0;
	}
?>