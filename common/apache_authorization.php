<?php
	function apache_authorization($name = '')
	{
		layout_clear();
		
		if($name || is_string($name))
		{
			if(!is_string($name))
				$name = '';
			
			if($name == '')
				$realm = '';
			else
				$realm = ' realm="'.cp1252_encode($name, true).'"';
				
			header('WWW-Authenticate: Basic'.$realm);
		}
		
		header('HTTP/1.1 401 Authorization Required');
		header('Content-Type: text/html;charset=utf-8');
		
		?>
			<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
			<html>
				<head>
					<title>401 Authorization Required</title>
				</head>
				<body>
					<h1>Authorization Required</h1>
					<p>This server could not verify that you are authorized to access the document requested.  Either you supplied the wrong credentials (e.g., bad password), or your browser doesn't understand how to supply the credentials required.</p>
					<hr><?=$_SERVER['SERVER_SIGNATURE']?>
				</body>
			</html>
		<?php
		
		die();
	}
?>