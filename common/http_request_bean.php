<?php
	class HttpRequest
	{
		var $server;
		var $path;
		var $data = null;
		var $port = 80;
		var $post = true;
		var $connection_timeout = 10;
		var $socket_timeout = 20;
		var $https = false;
		var $headers = null;
		var $redirect = false;
	}
?>