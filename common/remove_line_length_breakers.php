<?php
	function remove_line_length_breakers($text)
	{
		return removeLineLengthBreakers($text);
	}
	
	
	
	function removeLineLengthBreakers($text)
	{
		static $search = "/\n|\r|\t/";
		static $replacement = ' ';
		
		return preg_replace($search, $replacement, $text);
	}
?>