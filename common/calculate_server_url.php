<?php
	function calculate_server_url($include_password = false)
	{
		if($include_password && isset($_SERVER['PHP_AUTH_USER']))
		{
			$password = $_SERVER['PHP_AUTH_USER'].':'.$_SERVER['PHP_AUTH_PW'].'@';
		}
		else
			$password = '';
		
		return 'http'
			.(@$_SERVER['HTTPS']!==null ? 's' : '')
			.'://'
			.$password
			.(@$_SERVER['HTTP_HOST'] !== null ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_ADDR'])
			.(($_SERVER['SERVER_PORT'] != (@$_SERVER['HTTPS']!==null ? 463 : 80)) ? ':'.$_SERVER['SERVER_PORT'] : '')
		;
	}
?>