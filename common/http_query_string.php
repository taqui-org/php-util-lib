<?php
	function array_to_query_string($dados)
	{
		return arrayToQueryString($dados);
	}
	
	
	
	function arrayToQueryString($dados)
	{
		$queryString = '';
		
		if(gettype($dados) == 'array')
			foreach($dados as $nome => $valor)
			{
				if($queryString != '')
					$queryString .= '&';
				
				$queryString .= urlencode($nome).'='.urlencode($valor);
			}
		
		return $queryString;
	}
?>