<?php
	function apache_forbidden()
	{
		layout_clear();
		
		header('HTTP/1.1 403 Forbidden');
		header('Content-Type: text/html;charset=utf-8');
		
		?>
			<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
			<html>
				<head>
					<title>403 Forbidden</title>
				</head>
				<body>
					<h1>Forbidden</h1>
					<p>You don't have permission to access <?=toHTML(get_path())?> on this server.</p>
					<hr><?=$_SERVER['SERVER_SIGNATURE']?>
				</body>
			</html>
		<?php
		
		die();
	}
?>