<?php
	function &http_request($request)
	{
		return httpRequest($request);
	}
	
	
	
	function &httpRequest($request)
	{
		return httpSubmit($request->server, $request->path, $request->data, $request->port, $request->post, $request->connection_timeout, $request->socket_timeout, $request->https, $request->headers, $request->redirect);
	}
	
	
	
	function &url_request($url, $headers = null, $data = null, $connection_timeout = 10, $socket_timeout = 20, $redirect = false)
	{
		return urlRequest($url, $headers, $data, $connection_timeout, $socket_timeout, $redirect);
	}
	
	
	
	function &urlRequest($url, $headers = null, $data = null, $connection_timeout = 10, $socket_timeout = 20, $redirect = false)
	{
		$url = get_parsed_url($url);
		
		if(is_bool($data))
		{
			$redirect = $data;
			$data = null;
		}
		
		if($data === null && preg_match('/^#(.*)$/', $url->anchor, $matches))
			$data = $matches[1];
		
		$request = new HttpRequest();
		$request->server = $url->server;
		$request->path = $url->path . $url->query;
		$request->data = $data;
		$request->port = $url->port;
		$request->post = ($data !== null && $data !== '');
		$request->connection_timeout = $connection_timeout;
		$request->socket_timeout = $socket_timeout;
		$request->https = $url->https;
		$request->headers = $headers;
		$request->redirect = $redirect;
		
		if($url->user !== null)
		{
			if(!is_array($request->headers))
				$request->headers = array();
			
			$request->headers['Authorization'] = 'Basic '.base64_encode($url->user.':'.$url->password); 
		}
		
		return httpRequest($request);
	}
?>